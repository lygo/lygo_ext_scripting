package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"testing"
	"time"
)

func Test_nodemailer(t *testing.T) {
	const SCRIPT = `
	(function(){
		var test = require("./modules/test_nodemailer.js");
		return test.run();
	})();
	
	`

	expected := map[string]string{

	}

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	vm1 := lygo_ext_scripting.NewEngine()
	vm1.Name = "nodemailer" // creates log file vm1.log
	registry.Start(vm1)

	v, err := vm1.RunScript(SCRIPT)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	response := v.String()
	if lygo_regex.IsValidJsonObject(response) {
		m := make(map[string]interface{})
		err = lygo_json.Read(response, &m)
		if err != nil {
			t.Error(err)
		}

		for k, v := range expected {
			value := lygo_reflect.GetString(m, k)
			if value != v {
				t.Error("Expected: " + v + " but got " + value)
			}
		}
	} else {
		t.Error(response)
	}

	time.Sleep(5 * time.Second)
}
