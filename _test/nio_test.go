package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"errors"
	"testing"
	"time"
)

func Test_nio(t *testing.T) {
	const SCRIPT = `
	(function(){
		var test = require("./modules/test_nio.js");
		return test.run();
	})();
	
	`

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	expected := map[string]string{

	}

	err := runNio(0, registry, SCRIPT, expected)

	time.Sleep(1 * time.Second)

	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	time.Sleep(10 * time.Second)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func runNio(index int, registry *lygo_ext_scripting.ModuleRegistry, script string, expected map[string]string) error {
	vm := lygo_ext_scripting.NewEngine()
	vm.Name = "nio"
	vm.Silent = false
	registry.Start(vm)

	v, err := vm.RunScript(script)
	if err != nil {
		return err
	}
	// fmt.Println(v.String())

	m := make(map[string]interface{})
	err = lygo_json.Read(v.String(), &m)
	if err != nil {
		return err
	}

	for k, v := range expected {
		value := lygo_reflect.GetString(m, k)
		if value != v {
			return errors.New("Expected: " + v + " but got " + value)
		}
	}

	return nil
}
