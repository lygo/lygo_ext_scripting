package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/http/httpclient"
	"fmt"
	"sync"
	"testing"
)

func Test_runClient(t *testing.T) {
	client := httpclient.NewHttpClient()

	// test call
	url := "http://localhost/"
	status, data, err := client.Get(url)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(url+" STATUS:", status, "BYTES:", len(data))

	var wg sync.WaitGroup
	// process 300 connections
	for i := 0; i < 300; i++ {
		wg.Add(1)
		go func(i int) {
			defer func() {
				if r := recover(); r != nil {
					// recovered from panic
					message := lygo_strings.Format("[panic] Test_runClient: %s", r)
					// TODO: implement logger
					fmt.Println(message)
					wg.Done()
				}
			}()
			goClient := httpclient.NewHttpClient()
			url := "http://localhost/api/hello"
			goClient.AddHeader("Authorization", "Bearer 1234")
			status, data, err := goClient.Get(url)
			if nil != err {
				fmt.Println(i, "ERROR:", err)
			}
			fmt.Println(i, url+" STATUS:", status, "BYTES:", len(data), "TEXT", string(data))
			wg.Done()
		}(i)
	}
	wg.Wait()

}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
