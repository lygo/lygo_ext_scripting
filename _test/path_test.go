package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"testing"
	"time"
)

func Test_path_delimiter(t *testing.T) {
	const SCRIPT = `
	(function(){
		var test = require("./modules/test_path.js");
		return test.run();
	})();
	
	`

	expected := map[string]string{
		"dirname":     "/dir/dir2/dir3",
		"basename":    "file",
		"extname":     ".html",
		"format":      "/home/dir1/file.html",
		"isAbsolute":  "true",
		"isAbsolute2": "false",
		"join":        "/dir/dir2/dir3",
		"normalize":   "/foo/bar/baz/asdf",
		"parse":       "{\"base\":\"file.txt\",\"dir\":\"/foo/bar/baz/asdf/quux/\",\"ext\":\".txt\",\"name\":\"file\",\"root\":\"/\"}",
		"relative":    "../../impl/bbb",
		"resolve":     "/foo/bar/baz",
		"resolve2":     "/foo/bar/tmp/file", // differ from Node.js method
		"resolve3":     lygo_paths.Absolute("./") + "/wwwroot/static_files/gif/image.gif",
	}

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	vm1 := lygo_ext_scripting.NewEngine()
	vm1.Name = "vm1" // creates log file vm1.log
	registry.Start(vm1)

	v, err := vm1.RunScript(SCRIPT)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	// fmt.Println(v.String())

	m := make(map[string]interface{})
	err = lygo_json.Read(v.String(), &m)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	for k, v := range expected {
		value := lygo_reflect.GetString(m, k)
		if value != v {
			t.Error("Expected: " + v + " but got " + value)
			t.FailNow()
		}
	}

	time.Sleep(1 * time.Second)
}
