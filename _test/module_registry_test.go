package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"fmt"
	"testing"
	"time"
)

func TestRequire(t *testing.T) {
	const SCRIPT = `
	var m = require("./modules/m.js");
	m.test();
	`

	vm := lygo_ext_scripting.NewEngine()

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})
	registry.Start(vm)

	v, err := vm.RunScript(SCRIPT)
	if err != nil {
		t.Fatal(err)
	}

	if !v.StrictEquals(vm.ToValue("passed")) {
		t.Fatalf("Unexpected result: %v", v)
	}
}

func TestConsole(t *testing.T) {
	vm := lygo_ext_scripting.NewEngine()

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})
	registry.Start(vm)

	if c := vm.Get("console"); c == nil {
		t.Fatal("console not found")
	}

	if _, err := vm.RunScript("console.log('')"); err != nil {
		t.Fatal("console.log() error", err)
	}

	if _, err := vm.RunScript("console.error('')"); err != nil {
		t.Fatal("console.error() error", err)
	}

	if _, err := vm.RunScript("console.warn('')"); err != nil {
		t.Fatal("console.warn() error", err)
	}
}

func TestEventLoop(t *testing.T) {
	const SCRIPT = `
	var m = require("./modules/m.js");

	console.log("Prepare timer");
	setTimeout(function() {
		console.log("timer", "ok", m.test());
	}, 1000);
	console.log("Started");
	`

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	vm1 := lygo_ext_scripting.NewEngine()
	vm1.Name = "vm1"
	registry.Start(vm1)

	v, err := vm1.RunScript(SCRIPT)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println("Returned", v)

	vm2 := lygo_ext_scripting.NewEngine()
	vm2.Name = "vm2"
	registry.Start(vm2)
	v, err = vm2.RunScript(SCRIPT)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(10*time.Second)

}



