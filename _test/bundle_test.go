package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"fmt"
	"testing"
)

func Test_bundle(t *testing.T) {
	data, err := lygo_io.ReadBytesFromFile("./scripts/program1.min.js")
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	vm1 := lygo_ext_scripting.NewEngine()
	vm1.Name = "bundle" // creates log file bundle.log
	registry.Start(vm1)

	v, err := vm1.RunScript(string(data))
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	window := vm1.Get("runtime")
	vm1.Close()

	vExport := v.Export()
	fmt.Println("DIRECT RESPONSE", vExport)
	response := window.Export()
	fmt.Println("CONTEXT RESPONSE", response)

}