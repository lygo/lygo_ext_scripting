package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"errors"
	"testing"
)

func Test_httpClient(t *testing.T) {
	const SCRIPT = `
	(function(){
		var test = require("./modules/test_httpclient.js");
		return test.run();
	})();
	
	`

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	expected := map[string]string{

	}

	err := run(0, registry, "test_httpclient", SCRIPT, expected)
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func run(index int, registry *lygo_ext_scripting.ModuleRegistry, name string, script string, expected map[string]string) error {
	vm := lygo_ext_scripting.NewEngine()
	vm.Name = name
	vm.Silent = false
	registry.Start(vm)

	v, err := vm.RunScript(script)
	if err != nil {
		return err
	}
	vm.Close()
	// fmt.Println(v.String())

	m := make(map[string]interface{})
	valString := v.String()
	valInterface := v.Export()
	err = lygo_json.Read(valString, &m)
	if err != nil {
		err = lygo_json.Read(valInterface, &m)
		if err != nil {
			return err
		}
	}

	for k, v := range expected {
		value := lygo_reflect.GetString(m, k)
		if value != v {
			return errors.New("Expected: " + v + " but got " + value)
		}
	}

	return nil
}

