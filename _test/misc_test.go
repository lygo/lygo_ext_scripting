package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"errors"
	"testing"
	"time"
)

func Test_misc(t *testing.T) {
	const SCRIPT = `
	(function(){
		var test = require("./modules/test_misc.js");
		return test.run();
	})();
	
	`

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	expected := map[string]string{

	}

	err := run(0, registry, SCRIPT, expected)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	time.Sleep(1 * time.Second)
}

func Test_thread(t *testing.T) {
	const SCRIPT = `
	(function(){
		var test = require("./modules/test_misc.js");
		return test.run();
	})();
	
	`

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	expected := map[string]string{

	}

	for i:=0;i<100;i++{
		go func() {
			err := run(i, registry, SCRIPT, expected)
			if nil != err {
				panic(err)
			}
		}()
	}

	time.Sleep(5 * time.Second)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func run(index int, registry *lygo_ext_scripting.ModuleRegistry, script string, expected map[string]string) error {
	vm1 := lygo_ext_scripting.NewEngine()
	vm1.Name = "all" // creates log file vm1.log
	vm1.Silent = false
	registry.Start(vm1)

	v, err := vm1.RunScript(script)
	if err != nil {
		return err
	}
	// fmt.Println(v.String())

	m := make(map[string]interface{})
	err = lygo_json.Read(v.String(), &m)
	if err != nil {
		return err
	}

	for k, v := range expected {
		value := lygo_reflect.GetString(m, k)
		if value != v {
			return errors.New("Expected: " + v + " but got " + value)
		}
	}

	return nil
}
