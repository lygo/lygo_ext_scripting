package _test

import (
	"bitbucket.org/lygo/lygo_ext_scripting/commons"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/console"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/require"
	_ "bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/util"
	"github.com/dop251/goja"
	"testing"
	"time"
)

func TestRun(t *testing.T) {
	const SCRIPT = `
	setTimeout(function() {
		console.log("ok");
	}, 1000);
	console.log("Started");
	`

	vm := goja.New()
	ctx := &commons.RuntimeContext{
		Runtime: vm,
	}
	new(require.Registry).Enable(ctx)
	console.Enable(ctx)

	loop := commons.NewEventLoop(vm)

	prg, err := goja.Compile("main.js", SCRIPT, false)
	if err != nil {
		t.Fatal(err)
	}
	loop.Run(func(vm *goja.Runtime) {
		vm.RunProgram(prg)
	})
}

func TestStart(t *testing.T) {
	const SCRIPT = `
	setTimeout(function() {
		console.log("ok");
	}, 1000);
	console.log("Started");
	`

	prg, err := goja.Compile("main.js", SCRIPT, false)
	if err != nil {
		t.Fatal(err)
	}

	vm := goja.New()
	ctx := &commons.RuntimeContext{
		Runtime: vm,
	}
	new(require.Registry).Enable(ctx)
	console.Enable(ctx)
	loop := commons.NewEventLoop(vm)
	loop.Start()

	loop.RunOnLoop(func(vm *goja.Runtime) {
		vm.RunProgram(prg)
	})

	time.Sleep(2 * time.Second)
	loop.Stop()
}

func TestInterval(t *testing.T) {
	const SCRIPT = `
	var count = 0;
	var t = setInterval(function() {
		console.log("tick");
		if (++count > 2) {
			clearInterval(t);
		}
	}, 1000);
	console.log("Started");
	`

	vm := goja.New()
	ctx := &commons.RuntimeContext{
		Runtime: vm,
	}
	new(require.Registry).Enable(ctx)
	console.Enable(ctx)
	loop := commons.NewEventLoop(vm)

	prg, err := goja.Compile("main.js", SCRIPT, false)
	if err != nil {
		t.Fatal(err)
	}
	loop.Run(func(vm *goja.Runtime) {
		vm.RunProgram(prg)
	})
}
