package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"testing"
	"time"
)

func Test_messagequeue(t *testing.T) {
	const SCRIPT = `
	(function(){
		var test = require("./modules/test_messagequeue.js");
		return test.run();
	})();
	`

	registry := lygo_ext_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return lygo_io.ReadBytesFromFile(path)
	})

	vm1 := lygo_ext_scripting.NewEngine()
	vm1.Name = "messagequeue" // creates log file messagequeue.log
	registry.Start(vm1)

	v, err := vm1.RunScript(SCRIPT)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	// fmt.Println(v.String())

	m := make(map[string]interface{})
	err = lygo_json.Read(v.String(), &m)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}


	time.Sleep(1 * time.Second)
}
