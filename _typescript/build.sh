# cd _typescript || exit
echo "BUILDING..."
npm run dev
npm run build
echo "CLEAR DESTINATION FOLDER...."
rm -rfv ../_test/scripts/*
echo "COPY CONTENT"
cp -R build/bundle/. ../_test/scripts
echo "DONE!"