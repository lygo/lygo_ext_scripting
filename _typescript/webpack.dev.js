const path = require("path");
const merge = require("webpack-merge");
const common = require("./webpack.common.js");


module.exports = merge(common, {
    mode: "development",

    entry: {
        "program1": path.join(__dirname, "./src/program1/launcher.ts"),
        "program2": path.join(__dirname, "./src/program2/launcher.ts"),
    },

    devtool: "source-map"

});