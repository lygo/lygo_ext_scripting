class Launcher2 {

    run(): void {
        const root: any = runtime;
        root.context["response"] = "HELLO WORLD!";

        console.log("Launcher2.run()");
        console.log("runtime:", JSON.stringify(root));
    }
}

// ---------------------------------------------------------------
//  l a u n c h e r
// ---------------------------------------------------------------

try {
    const launcher = new Launcher2();
    launcher.run();
} catch (err) {
    console.error("Launcher2: ", err);
}

