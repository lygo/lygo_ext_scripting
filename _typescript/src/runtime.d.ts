interface ScriptingRequireFunction {
    /* tslint:disable-next-line:callable-types */
    (id: string): any;
}
declare var require: ScriptingRequireFunction;
declare var use: ScriptingRequireFunction;
declare var runtime:any;


