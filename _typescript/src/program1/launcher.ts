
class Launcher1 {

    run(): void {
        const root: any = runtime;
        const path = use("path");
        const fs = use("fs");

        runtime.context["response"] = "HELLO WORLD: " + path.resolve('/dir1/dir2', './dir3');

        console.log("Launcher1.run()");
        console.log("runtime:", JSON.stringify(root));
    }
}

// ---------------------------------------------------------------
//  l a u n c h e r
// ---------------------------------------------------------------

try {
    const launcher = new Launcher1();
    launcher.run();
} catch (err) {
    console.error("Launcher1: ", err);
}

