const path = require("path");

module.exports = {

    entry: {
        // popup: path.join(__dirname, "src/popup/index.tsx"),
        // "background": path.join(__dirname, "src/launcher.ts"),
        // "background.min": path.join(__dirname, "src/launcher.ts")
    },

    output: {
        path: path.join(__dirname, "build"),
        filename: (context) => {
            return "bundle/[name].js";
        }
    },

    plugins: [

    ],

    module: {
        rules: [
            {
                exclude: /node_modules/,
                use: "ts-loader"
            },
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    },


};