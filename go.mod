module bitbucket.org/lygo/lygo_ext_scripting

go 1.16

require (
	4d63.com/gochecknoglobals v0.0.0-20210416044342-fb0abda3d9aa // indirect
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_email v0.1.6
	bitbucket.org/lygo/lygo_ext_2dcode v0.1.2
	bitbucket.org/lygo/lygo_ext_auth0 v0.1.22
	bitbucket.org/lygo/lygo_ext_dbal v0.1.31
	bitbucket.org/lygo/lygo_ext_http v0.1.20
	bitbucket.org/lygo/lygo_ext_logs v0.1.9
	bitbucket.org/lygo/lygo_ext_mq v0.1.8
	bitbucket.org/lygo/lygo_ext_sms v0.1.5
	bitbucket.org/lygo/lygo_nio v0.1.5
	bitbucket.org/lygo/lygo_num2word v0.1.2
	github.com/Djarvur/go-err113 v0.1.0 // indirect
	github.com/arangodb/go-driver v0.0.0-20210825071748-9f1169c6a7dc
	github.com/ashanbrown/forbidigo v1.2.0 // indirect
	github.com/bombsimon/wsl/v3 v3.3.0 // indirect
	github.com/cbroglie/mustache v1.2.2
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/chavacava/garif v0.0.0-20210405164556-e8a0a408d6af // indirect
	github.com/dop251/goja v0.0.0-20210904102640-6338b3246846
	github.com/esimonov/ifshort v1.0.2 // indirect
	github.com/fasthttp/websocket v1.4.3 // indirect
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/go-critic/go-critic v0.5.7 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/gofiber/websocket/v2 v2.0.10 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/golangci/golangci-lint v1.42.1 // indirect
	github.com/golangci/misspell v0.3.5 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/gordonklaus/ineffassign v0.0.0-20210729092907-69aca2aeecd0 // indirect
	github.com/gostaticanalysis/analysisutil v0.7.1 // indirect
	github.com/gostaticanalysis/forcetypeassert v0.1.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jingyugao/rowserrcheck v1.1.0 // indirect
	github.com/jirfag/go-printf-func-name v0.0.0-20200119135958-7558a9eaa5af // indirect
	github.com/julz/importas v0.0.0-20210419104244-841f0c0fe66d // indirect
	github.com/klauspost/compress v1.13.5 // indirect
	github.com/kyoh86/exportloopref v0.1.8 // indirect
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mbilski/exhaustivestruct v1.2.0 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/polyfloyd/go-errorlint v0.0.0-20210903085826-e4f368f0ae69 // indirect
	github.com/prometheus/common v0.30.0 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/quasilyte/regex/syntax v0.0.0-20210819130434-b3f0c404a727 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/sanposhiho/wastedassign/v2 v2.0.7 // indirect
	github.com/savsgio/gotils v0.0.0-20210907153846-c06938798b52 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	github.com/spf13/cast v1.4.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/tdakkota/asciicheck v0.0.0-20200416200610-e657995f937b // indirect
	github.com/tetafro/godot v1.4.10 // indirect
	github.com/timakin/bodyclose v0.0.0-20210704033933-f49887972144 // indirect
	github.com/valyala/fasthttp v1.30.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/mod v0.5.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/ini.v1 v1.63.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	mvdan.cc/gofumpt v0.1.1 // indirect
	mvdan.cc/unparam v0.0.0-20210701114405-894c3c7ee6a6 // indirect
)
