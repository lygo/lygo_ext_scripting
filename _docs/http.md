# Http #

Http is a module that implements both client and server http.

Sample usage:

```javascript
var http = require("http");

var client = http.newClient();
// do something with client....

var server = http.newServer();
// do something with server....
```

## Http Client ##

```javascript
var http = require("http");

var client = http.newClient();
var res = client.get("http://gianangelogeminiani.me");

console.log(JSON.stringify(res));
console.log(res.text());
```

### Client Methods ###

* get: Submit a "GET" request to URL. Return a Http Response object.

## Http Server ##

Http Server is a minimal and flexible web application framework that provides 
a robust set of features for web and mobile applications.

```javascript
var http = require("http");

var app = http.newServer();

// configure static files 
app.static({
    "enabled": true,
    "prefix": "/",
    "root": "./www", // http root
    "index": "",
    "compress": true
});

// routing with a simple API
app.get("/api/:name", onRoute);

// websocket support
app.websocket("/websocket", onWebsocket);

// open channels and start listening 
app.listen([{
    "addr": "80",
    "tls": false
}, {
    "addr": "443",
    "tls": true,
    "ssl_cert": "./cert/ssl-cert.pem",
    "ssl_key": "./cert/ssl-cert.key"
}]);

function onRoute(req, res) {
    var data = {
        "message": "This is a response to: " + req.originalUrl,
        "query": req.query,
        "params": req.params,
        "name": req.param("name"),
        "path": req.path,
        "originalUrl": req.originalUrl,
        "baseUrl": req.baseUrl
    }
    res.json(data);
}

function onWebsocket(req, res) {
    try{
        var data = {
            "params": req.params,
            "uid": req.param("uid")
        }
        console.log("websocket request", req.text());
        console.log("data", JSON.stringify(data));

        // return data
        res.json(data);
    }catch(err){
        console.error("onWebsocket", err);
    }
}


// lock script and wait server is closed
app.join();
```

**Sample File Upload**

```javascript
var http = require("http");
var app = http.newServer();

// configure static files 
app.static({
    "enabled": true,
    "prefix": "/",
    "root": "./www", // http root
    "index": "",
    "compress": true
});

// routing
app.get("/upload/*", onUpload);

// open channels and start listening 
app.listen([{
    "addr": "80",
    "tls": false
}, {
    "addr": "443",
    "tls": true,
    "ssl_cert": "./cert/ssl-cert.pem",
    "ssl_key": "./cert/ssl-cert.key"
}]);

// upload handler
function onUpload(req, res) {
    var response = {
        "error": undefined,
        "response": []
    };
    try {
        // get multipart form with data and files
        var form = req.multipart();
        if (!!form) {
            // console.log("params: " + JSON.stringify(form.data));
            // console.log("files: " + form.files.length);
            for (var i = 0; i < form.files.length; i++) {
                var file = form.files[i]
                var pathLevel = 3; // save file under "./YEAR/MONTH/DAY/" path
                var max = 2100000; // max size = 2Mb
                var path = file.save("./uploads/", pathLevel, max); // save file under "./uploads/YEAR/MONTH/DAY/" path
                // add file system path
                response.response.push(path);
                // console.log("SAVED TO: " + path);
            }
        } else {
            // console.error("NOTHING FROM UPLOAD REQUEST")
            response.error = "NOTHING FROM UPLOAD REQUEST";
        }
    } catch (err) {
        // console.log(err);
        response.error = err.message;
    }
    if (!!response.error) {
        res.status(403);
    } else {
        res.json(response);
    }
}
```

## Request ##
The req object represents the HTTP request and has properties for the request query string, 
parameters, body, HTTP headers, and so on. 

In this documentation and by convention, the object is always referred to as req (and the HTTP response is res) but 
its actual name is determined by the parameters to the callback function in which you’re working.

For example:

```javascript
app.get('/user/:id', function (req, res) {
  res.send('user ' + req.params.id)
})
```

But you could just as well have:

```javascript
app.get('/user/:id', function (request, response) {
  response.send('user ' + request.params.id)
})
```

###Properties:


**req.originalUrl**

This property retains the original request URL.

```javascript
// GET /search?q=something
console.log(req.originalUrl)
// => '/search?q=something'
```

In a middleware function, req.originalUrl is a combination of req.baseUrl and req.path, as shown in the following example.

```javascript
app.use('/admin', function (req, res, next) { // GET 'http://www.example.com/admin/new'
  console.dir(req.originalUrl) // '/admin/new'
  console.dir(req.baseUrl) // '/admin'
  console.dir(req.path) // '/new'
  next()
})
```

## Response ##

The res object represents the HTTP response that an app sends when it gets an HTTP request.

In this documentation and by convention, the object is always referred to as res (and the HTTP request is req) but its 
actual name is determined by the parameters to the callback function in which you’re working.

For example:

```javascript
app.get('/user/:id', function (req, res) {
  res.send('user ' + req.params.id)
})
```

But you could just as well have:

```javascript
app.get('/user/:id', function (request, response) {
  response.send('user ' + request.params.id)
})
```

Properties: 

* size: (*Number*) Size of response
* statusCode: (*Number*) Status Code of HTTP response. i.e. 200
* statusMessage: (*String*) Message of Status. i.e. "OK"
* status: (*String*) Complete Status Message. i.e. "200:OK"

Methods:

* length(): (*Number*) Return the size of response
* bytes(): (*Array of Bytes*) Return response as bytes
* text(): (*String*) Return response as text
* send (data): (Available only in server callbacks) Write a response to client
* json (data): (Available only in server callbacks) Write a JSON response to client

