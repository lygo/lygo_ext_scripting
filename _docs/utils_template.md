## template-utils ##

```javascript
var path = require("path");
var template = require("template-utils");

var data = {
    "user" :{
        "name":"Mario"
    }
};
var text = template.mergeFile(path.resolve("./dir/file.html"), data);
console.log(text);
```