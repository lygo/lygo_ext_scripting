# date-utils #

```javascript

    const dt = require('date-utils');

    // current date-time
    current = dt.wrap();
    response.current = current.format(); // format with default pattern (ISO)

    // js native Date
    jsdate = dt.wrap(new Date());
    response.jsdate = jsdate.format(); // format with default pattern (ISO)

    sdate = dt.wrap("2020-09-01 12:15");
    response.sdate = sdate.format("yyyyMMdd"); // result is: 20200901

```

## Format Patterns ##

* `y` = Year
* `M` = Month
* `d` = Day
* `H` = Hour
* `m` = Minute
* `s` = Second
* `Z` = Time Zone

Example: `"yyyyMMdd HH:mm:ss Z"`

## Methods ##

### wrap ###

Wrap a date and return a dateparser object.

```javascript
 var current = dt.wrap();
 var other = dt.wrap(new Date());   
 var more = dt.wrap("2020-01-01");
    
```

### getLayout ###

Analyze a string date and return detected layout.

```javascript
 var layout = dt.getLayout('01/01/2020 10:15');
 
    
```