# Sys #

`sys` is an utility module to access some Operating System functions.

Sample usage:

```javascript
var sys = require("sys");

var unique_machine_id = sys.id();
var sys_info = sys.getInfo();

```