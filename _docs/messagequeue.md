## messagequeue ##

Message Queue (RabbitMQ) client implementation.

**SAMPLE USAGE (CONSUMER)**

```javascript
var mq = use("message-queue");

try {
    // connect to broker
    const conn = mq.newConnection({
        "protocol": "amqp",
        "secret": "1234567890", // optional. If assigned messages are encrypted
        "url": "amqp://test:test@localhost:5672/"
    });

    // test broker connection
    if (!!conn && conn.ping()) {

        conn.notifyClose(function(){
            console.log("DISCONNECTED!!!");
        });
        
        // declare an exchange (optional, you can send messages also directly at queue)
        try {
            conn.exchangeDeclare({
                "passive": false,
                "name": "MyExchange",
                "kind": "direct",
                "durable": true,
                "auto-delete": false,
                "internal": false,
                "no-wait": false,
                "args": null
            });
        } catch (err) {
            console.error("Error declaring Exchange:", err);
        }

        // declare a queue
        try {
            var queue = conn.queueDeclare({
                "passive": false,
                "name": "MyQueue",
                "durable": false,
                "exclusive": false,
                "auto-delete": false,
                "no-wait": false,
                "args": null
            });
            console.log("Queue Declared:", JSON.stringify(queue));

            // bind exchange and queue
            try{
                conn.queueBind({
                    "name": queue.name,
                    "exchange": "MyExchange",
                    "key": "only-some-messages",
                    "no-wait": false,
                    "args": null
                });

                // consume messages
                try {
                    var consumer = conn.newListener({
                        "queue": queue.name,
                        "consumer-tag": "only-some-messages",
                        "no-local": false,
                        "auto-ack": false,
                        "exclusive": false,
                        "no-wait": false,
                        "args": null
                    });
                    consumer.listen(function (message) {
                        console.log("receiving:", JSON.stringify(message))
                    });
                } catch (err) {
                    console.error("Error creating new consumer:", err);
                }
            }catch(err){
                console.error("Error Binding queue:", err);
            }
        } catch (err) {
            console.error("Error declaring Queue:", err);
        }
    } else {
        console.error("Connection not available. This should not happen immediatelly after a connection creation.");
    }
} catch (err) {
    console.error("Broker not available:", err);
}
```

**SAMPLE USAGE (EMITTER)**

```javascript
var mq = use("message-queue");

try {
    // connect to broker
    var conn = mq.newConnection({
        "protocol": "amqp",
        "secret": "1234567890", // optional. If assigned messages are encrypted
        "url": "amqp://test:test@localhost:5672/"
    });

    // test broker connection
    if (conn.ping()) {

        // declare an exchange
        try {
            conn.exchangeDeclare({
                "passive": false,
                "name": "MyExchange",
                "kind": "direct",
                "durable": true,
                "auto-delete": false,
                "internal": false,
                "no-wait": false,
                "args": null
            });
        } catch (err) {
            console.error("Error declaring Exchange:", err);
        }

        // emit messages
        try {
            var emitter = conn.newEmitter({
                "exchange": "MyExchange",
                "key": "only-some-messages",
                "mandatory": false,
                "immediate": true
            });
            try {
                emitter.emit({
                    "content-type": "text/plain",
                    "body": "HELLO WORLD!"
                });
            } catch (err) {
                console.error("Error emitting a message:", err);
            }
        } catch (err) {
            console.error("Error creating new emitter:", err);
        }
    } else {
        console.error("Connection not available. This should not happen immediatelly after a connection creation.");
    }
} catch (err) {
    console.error("Broker not available:", err);
}
```