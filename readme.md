# Scripting #

![icon](./icon.png)

Scripting is a Javascript engine with "modules" implementation.

## How to Use ##

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_scripting
```

## Sample ##
```javascript
var path = use("path");

(function(){
	
    var response = {
        delimiter: path.delimiter,
        sep: path.sep,
        dirname: path.dirname("/dir/dir2/dir3/file"),
        basename: path.basename("/dir/dir2/dir3/file.html", ".html"),
        extname: path.extname("/dir/dir2/dir3/file.html"),
        format: path.format({
            "dir":"/home/dir1",
            "base":"file.html"
        }),
        isAbsolute: path.isAbsolute("/dir/dir2/dir3/file.html"),
        isAbsolute2: path.isAbsolute("file.html"),
        join: path.join("/dir/dir2", "dir3", "file.html", ".."),
        normalize: path.normalize('/foo/bar//baz/asdf/quux/..'),
        parse: path.parse('/foo/bar/baz/asdf/quux/file.txt'),
        relative: path.relative('/data/orandea/test/aaa', '/data/orandea/impl/bbb'),
        resolve: path.resolve('/foo/bar', './baz'),
        resolve2: path.resolve('/foo/bar', '/tmp/file/'),
        resolve3: path.resolve('wwwroot', 'static_files/png/', '../gif/image.gif'),

    };

    var s = JSON.stringify(response);
    console.debug(s);
    return s;

})();
```

## Modules ##

* [auth0](_docs/auth.md) : JWT support with SignIn and SignUp helpers
* [dbal](_docs/dbal.md): Database Abstraction Layer. Support MySQL, ArangoDB
* [fs](_docs/fs.md) : File System Module
* [http](_docs/http.md) : Http Client and Http Server (Express like) implementation
* [line-reader](_docs/linereader.md): Utility to read text files line-by-line
* [nio](_docs/nio.md): Client/Server Network I/O 
* [nodemailer](_docs/nodemailer.md): Send Emails
* [path](_docs/path.md): Path utilities
* [sql](_docs/sql.md): SQL implementation for MySQL and all supported drivers
* [sys](_docs/sys.md): System utilities and information
* [crypto-utils](_docs/utils_crypto.md): Utility for cryptography
* [file-utils](_docs/utils_file.md): Utility for file R/W
* [date-utils](_docs/utils_date.md): Utility for date parsing, formatting and manipulation
* [template-utils](_docs/utils_template.md): Utility for template merging
* [rnd-utils](_docs/utils_rnd.md): Utility for Random values generation

## How To Write Your Own Modules ##

TODO: write documentation....

## Dependencies ##

This module depend on [Goja](https://github.com/dop251/goja) 

`go get -u github.com/dop251/goja/...`

And some LyGo Modules:

```
go get -u bitbucket.org/lygo/lygo_commons

go get -u bitbucket.org/lygo/lygo_num2word

go get -u bitbucket.org/lygo/lygo_ext_logs

go get -u bitbucket.org/lygo/lygo_nio

go get -u bitbucket.org/lygo/lygo_nio

go get -u bitbucket.org/lygo/lygo_ext_auth0

go get -u bitbucket.org/lygo/lygo_ext_sms

```

### SQL Dependencies ###

`go get -u github.com/go-sql-driver/mysql`

### NoSQL Dependencies ###

**ArangoDB:**

```
go get github.com/arangodb/go-driver
go get github.com/arangodb/go-driver/http
```


### Versioning ##

Sources are versioned using git tags:

```
git tag v0.1.103
git push origin v0.1.103
```

Latest Version:
```
go get -u bitbucket.org/lygo/lygo_ext_scripting@v0.1.103
```