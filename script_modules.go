package lygo_ext_scripting

import (
	"bitbucket.org/lygo/lygo_ext_scripting/commons"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/auth0"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/dbal"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/console"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/process"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/require"
	_ "bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/util"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/window"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/elasticsearch"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/fs"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/http"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/linereader"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/message_queue"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/nio"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/nodemailer"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/nosql"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/path"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/showcase_engine"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/smssender"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/sql"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/sys"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/utils/barcodeutils"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/utils/cryptoutils"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/utils/dateutils"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/utils/executils"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/utils/fileutils"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/utils/rndutils"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/utils/templateutils"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type ModuleRegistry struct {
	loader   require.SourceLoader
	registry *require.Registry
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewModuleRegistry(loader require.SourceLoader) *ModuleRegistry {
	instance := new(ModuleRegistry)
	instance.loader = loader
	instance.registry = require.NewRegistryWithLoader(loader)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ModuleRegistry) Start(engine *ScriptEngine) *commons.RuntimeContext {
	context := &commons.RuntimeContext{
		Uid:       &engine.Name,
		Workspace: engine.Root,
		Runtime:   engine.runtime,
		Arguments: []interface{}{
			&engine.Root, &engine.Name, &engine.Silent, &engine.LogLevel,
			&engine.ResetLogOnEachRun, engine.GetLogger,
		},
	}

	// creates context registry
	instance.registry.Enable(context)

	// start engine if not already started
	engine.Open()

	// add support to console and other defaults
	console.Enable(context)
	process.Enable(context)
	window.Enable(context)

	//-- add modules --//

	// auth0
	auth0.Enable(context)
	// dbal
	dbal.Enable(context)
	// elastic search
	elasticsearch.Enable(context)
	// showcase engine
	showcase_engine.Enable(context)
	// file system (Nodejs clone)
	fs.Enable(context)
	// http
	http.Enable(context)
	// line text reader
	linereader.Enable(context)
	// message queue (RabbitMQ)
	message_queue.Enable(context)
	// network io
	nio.Enable(context)
	// email sender
	nodemailer.Enable(context)
	// nosql
	nosql.Enable(context)
	// path (Nodejs clone)
	path.Enable(context)
	// sms sender
	smssender.Enable(context)
	// sql layer
	sql.Enable(context)
	// system
	sys.Enable(context)
	// utility
	barcodeutils.Enable(context)
	cryptoutils.Enable(context)
	dateutils.Enable(context)
	executils.Enable(context)
	fileutils.Enable(context)
	rndutils.Enable(context)
	templateutils.Enable(context)

	return context
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
