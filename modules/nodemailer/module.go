package nodemailer

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_email"
	"bitbucket.org/lygo/lygo_ext_scripting/commons"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/require"
	"crypto/tls"
	"errors"
	"fmt"
	"github.com/dop251/goja"
	"io/ioutil"
	"net/http"
	"net/mail"
	"net/smtp"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "nodemailer"

type Path struct {
	runtime *goja.Runtime
	util    *goja.Object
	root    string
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// nodemailer.createTransport(options[, defaults])
func (instance *Path) createTransport(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) > 0 {
		options := call.Argument(0).Export()
		if o, b := options.(map[string]interface{}); b {
			// creates transport object
			transport := &map[string]interface{}{
				"sendMail": instance.sendMail(o),
			}
			return instance.runtime.ToValue(transport)
		} else {
			// invalid options
			panic(instance.runtime.NewTypeError("Invalid Options."))
		}
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Path) sendMail(options map[string]interface{}) func(call goja.FunctionCall) goja.Value {
	return func(call goja.FunctionCall) goja.Value {
		if nil != options && len(call.Arguments) == 2 {
			host := lygo_reflect.GetString(options, "host")
			port := lygo_reflect.GetInt(options, "port")
			secure := lygo_reflect.GetBool(options, "secure")
			user := lygo_reflect.GetString(lygo_reflect.Get(options, "auth"), "user")
			pass := lygo_reflect.GetString(lygo_reflect.Get(options, "auth"), "pass")
			if len(host) > 0 && port > 0 && len(user) > 0 && len(pass) > 0 {
				message := call.Argument(0).Export()
				callback, _ := goja.AssertFunction(call.Argument(len(call.Arguments) - 1))
				if nil != message && nil != callback {
					from := lygo_reflect.GetString(message, "from")
					to := lygo_reflect.GetString(message, "to")
					subject := lygo_reflect.GetString(message, "subject")
					text := lygo_reflect.GetString(message, "text")
					html := lygo_reflect.GetString(message, "html")
					attachments := lygo_reflect.GetArray(message, "attachments")
					if len(from) > 0 && len(to) > 0 && len(subject) > 0 && (len(text) > 0 || len(html) > 0) {
						err := sendEmail(host, port, secure, user, pass, from, to, subject, text, html, attachments)
						if nil != err {
							_, _ = callback(call.This, instance.runtime.ToValue(err.Error()), goja.Undefined())
						} else {
							info := &map[string]interface{}{}
							_, _ = callback(call.This, goja.Undefined(), instance.runtime.ToValue(info))
						}
					} else {
						panic(instance.runtime.NewTypeError("Missing Parameters in Message Object."))
					}
				} else {
					panic(instance.runtime.NewTypeError("Missing Message Object."))
				}
			}
		}
		return goja.Undefined()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

// https://nodemailer.com/message/attachments/
func sendEmail(host string, port int, secure bool, user string, pass string,
	from string, to string, subject string, text string, html string, attachments []interface{}) (err error) {
	servername := fmt.Sprintf("%v:%v", host, port)
	auth := smtp.PlainAuth("", user, pass, host)
	toList := lygo_strings.Split(to, ";,")
	var m *lygo_email.Message
	if len(html) > 0 {
		m = lygo_email.NewHTMLMessage(subject, html)
	} else {
		m = lygo_email.NewMessage(subject, text)
	}
	addr, err := mail.ParseAddress(from)
	if nil != err {
		m.From = &mail.Address{Address: from}
	} else {
		m.From = addr
	}
	m.To = toList
	for _, attachment := range attachments {
		if nil != attachment {
			if v, b := attachment.(string); b {
				err = addAttachmentString(m, v)
			} else if v, b := attachment.(map[string]interface{}); b {
				err = addAttachmentObject(m, v)
			}
		}
	}
	if nil != err {
		return
	}
	if secure {
		// TLS config
		tlsconfig := &tls.Config{
			InsecureSkipVerify: true,
			ServerName:         host,
		}
		err = lygo_email.SendSecure(servername, auth, tlsconfig, m)
		return
	} else {
		err = lygo_email.Send(servername, auth, m)
		return
	}
}

func addAttachmentString(m *lygo_email.Message, attachment string) error {
	filename := lygo_paths.FileName(attachment, true)
	return addAttachment(m, filename, attachment)
}

func addAttachmentObject(m *lygo_email.Message, attachment map[string]interface{}) error {
	filename := lygo_reflect.GetString(attachment, "filename")
	path := lygo_reflect.GetString(attachment, "path")
	return addAttachment(m, filename, path)
}

func addAttachment(m *lygo_email.Message, filename, path string) error {
	if len(filename) > 0 && len(path) > 0 {
		data, err := download(path)
		if nil != err {
			return err
		}
		return m.AddAttachmentBinary(filename, data, false)
	}
	return nil // nothing to attach
}

func download(url string) ([]byte, error) {
	if len(url) > 0 {
		if strings.Index(url, "http") > -1 {
			// HTTP
			tr := &http.Transport{
				MaxIdleConns:       10,
				IdleConnTimeout:    15 * time.Second,
				DisableCompression: true,
			}
			client := &http.Client{Transport: tr}
			resp, err := client.Get(url)
			if nil == err {
				defer resp.Body.Close()
				body, err := ioutil.ReadAll(resp.Body)
				if nil == err {
					return body, nil
				} else {
					return []byte{}, err
				}
			} else {
				return []byte{}, err
			}
		} else {
			// FILE SYSTEM
			path := url
			return lygo_io.ReadBytesFromFile(path)
		}
	}
	return []byte{}, lygo_errors.Prefix(errors.New(url), "Invalid url or path: ")
}

func load(runtime *goja.Runtime, module *goja.Object, args ...interface{}) {
	instance := &Path{
		runtime: runtime,
	}

	if len(args) > 0 {
		root := lygo_reflect.ValueOf(args[0]).String()
		if len(root) > 0 {
			instance.root = root
		}
	}

	o := module.Get("exports").(*goja.Object)
	_ = o.Set("createTransport", instance.createTransport)

}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
