package fileutils

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_scripting/commons"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/require"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "file-utils"

type FileUtils struct {
	runtime *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// utils.fileReadText(path)
func (instance *FileUtils) fileReadText(call goja.FunctionCall) goja.Value {
	path := call.Argument(0).String()
	text, err := lygo_io.ReadTextFromFile(path)
	if nil != err {
		// throw back error to javascript
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(text)
}

// utils.fileReadBytes(path)
func (instance *FileUtils) fileReadBytes(call goja.FunctionCall) goja.Value {
	path := call.Argument(0).String()
	bytes, err := lygo_io.ReadBytesFromFile(path)
	if nil != err {
		// throw back error to javascript
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(bytes)
}

// utils.fileWrite(path, data)
func (instance *FileUtils) fileWrite(call goja.FunctionCall) goja.Value {
	path := call.Argument(0).String()
	data := call.Argument(1).Export()
	err := WriteDataToFile(path, data)
	if nil != err {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func WriteDataToFile(path string, data interface{}) error {
	if bytes, b := data.([]byte); b {
		_, err := lygo_io.WriteBytesToFile(bytes, path)
		return err
	} else if text, b := data.(string); b {
		_, err := lygo_io.WriteTextToFile(text, path)
		return err
	}
	return nil
}

func load(runtime *goja.Runtime, module *goja.Object, _ ...interface{}) {
	instance := &FileUtils{
		runtime: runtime,
	}

	o := module.Get("exports").(*goja.Object)
	// file utility
	_ = o.Set("fileReadText", instance.fileReadText)
	_ = o.Set("fileReadBytes", instance.fileReadBytes)
	_ = o.Set("fileWrite", instance.fileWrite)

}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
