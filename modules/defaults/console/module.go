package console

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_logs"
	"bitbucket.org/lygo/lygo_ext_scripting/commons"
	"bitbucket.org/lygo/lygo_ext_scripting/modules/defaults/require"
	"errors"
	"fmt"
	"github.com/dop251/goja"
	"sync"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "console"

type Console struct {
	runtime  *goja.Runtime
	util     *goja.Object
	name     string
	filename string
	logger   lygo_ext_logs.ILogger
	silent   bool // enable/disable console output
	mux      sync.Mutex
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

func (instance *Console) log(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(lygo_ext_logs.LEVEL_INFO, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

func (instance *Console) error(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(lygo_ext_logs.LEVEL_ERROR, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

func (instance *Console) warn(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(lygo_ext_logs.LEVEL_WARN, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

func (instance *Console) info(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(lygo_ext_logs.LEVEL_INFO, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

func (instance *Console) debug(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(lygo_ext_logs.LEVEL_DEBUG, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Console) format(call goja.FunctionCall) (string, error) {
	if format, ok := goja.AssertFunction(instance.util.Get("format")); ok {
		ret, err := format(instance.util, call.Arguments...)
		if err != nil {
			return "", err
		}
		message := ret.String()

		return message, nil
	} else {
		return "", errors.New("util.format is not a function")
	}
}

func (instance *Console) write(level int, message string) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] MODULE %s ERROR: %s", NAME, r)
			fmt.Println(message)
		}
	}()

	if nil != instance.logger {
		switch level {
		case lygo_ext_logs.LEVEL_WARN:
			instance.logger.Warn(message)
		case lygo_ext_logs.LEVEL_INFO:
			instance.logger.Info(message)
		case lygo_ext_logs.LEVEL_ERROR:
			instance.logger.Error(message)
		case lygo_ext_logs.LEVEL_DEBUG:
			instance.logger.Debug(message)
		case lygo_ext_logs.LEVEL_TRACE:
			instance.logger.Trace(message)
		case lygo_ext_logs.LEVEL_PANIC:
			instance.logger.Panic(message)
		}
	} else {
		if len(instance.filename) > 0 {
			if b, _ := lygo_paths.Exists(instance.filename); !b {
				lygo_paths.Mkdir(instance.filename)
			}
			_, _ = lygo_io.AppendTextToFile(message+"\n", instance.filename)
		}
	}

	if !instance.silent {
		fmt.Println("["+instance.name+"] ", message)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func load(runtime *goja.Runtime, module *goja.Object, args ...interface{}) {
	instance := &Console{
		runtime: runtime,
	}
	instance.util = require.Require(runtime, "util").(*goja.Object)

	if len(args) > 3 {
		root := lygo_reflect.ValueOf(args[0]).String()
		name := lygo_reflect.ValueOf(args[1]).String()
		silent := lygo_reflect.ValueOf(args[2]).Bool()
		level := int(lygo_reflect.ValueOf(args[3]).Int())
		resetLog := lygo_reflect.ValueOf(args[4]).Bool()
		getEngine := args[5]

		if len(name) > 0 && len(root) > 0 {
			instance.name = name
			instance.silent = silent
			if f, b := getEngine.(func() lygo_ext_logs.ILogger); b {
				instance.logger = f()
				if l, b := instance.logger.(*lygo_ext_logs.Logger); b {
					instance.filename = l.GetFileName()
				}
			}
			if nil == instance.logger {
				instance.filename = lygo_paths.Concat(lygo_paths.Absolute(root), "logging", name+".log")
				// reset log
				if resetLog {
					_ = lygo_io.Remove(instance.filename)
				}
				logger := lygo_ext_logs.NewLogger()
				logger.SetOutput(lygo_ext_logs.OUTPUT_FILE)
				logger.SetFileName(instance.filename)
				logger.SetLevel(level)
				instance.logger = logger
			}
		}
	}

	o := module.Get("exports").(*goja.Object)
	_ = o.Set("log", instance.log)
	_ = o.Set("error", instance.error)
	_ = o.Set("warn", instance.warn)
	_ = o.Set("debug", instance.debug)
	_ = o.Set("info", instance.info)
}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})

	// add module to javascript context
	ctx.Runtime.Set(NAME, require.Require(ctx.Runtime, NAME))
}
